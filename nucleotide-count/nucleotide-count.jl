"""
    count_nucleotides(strand)

The frequency of each nucleotide within `strand` as a dictionary.

Invalid strands raise a `DomainError`.

"""

function count_nucleotides(strand)
    ## Check whether strand is valid:
    if occursin(r"[^ATCG]", strand)
        throw(DomainError(strand, "strand must contain only A, C, G or T."))
    end

    ## Count each nucleotide:
    counts = Dict('A' => 0, 'C' => 0, 'G' => 0, 'T' => 0)
    for char in strand
        counts[char] += 1
    end

    ## Return final result:
    return counts
end
